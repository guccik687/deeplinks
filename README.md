# Deep Links for LabCoat

Open links to self-hosted Gitlab instances in LabCoat

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/com.nomadlabs.labcoat.deeplinks/)
[<img src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png"
     alt="Get it on Google Play"
     height="80">](https://play.google.com/store/apps/details?id=com.nomadlabs.labcoat.deeplinks)

## Usage

1. Install LabCoat and log in to your gitlab instance
2. Install _Deep Links for LabCoat_
3. Links to supported gitlab instances will open in LabCoat

## Supported Domains

- Socket Mobile - git.socketmobile.com
- debian - salsa.debian.org
- freedesktop - gitlab.freedesktop.org
- Gnome - gitlab.gnome.org
- GNU - git.gnu.io

## Known Issues

**LabCoat opens but link does not load**

Make sure you are signed in to an account on the Gitlab instance you are trying
to access. LabCoat is unable to display anything from a Gitlab instance without
a valid personal access token - even if the url you are trying to access is
public on the web.

_Solution_: Open the link with your browser instead.

If you accidentally set _Deep Links_ as the default handler for a domain on
which you do not have a Gitlab account, go to _App info > Open by default > 
Clear defaults_
